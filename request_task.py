#!/usr/bin/python
# -*- coding: UTF-8 -*-
import os
import threading

import datetime
import time

import io
import pandas
import requests

from ding_talk import DingTalkMsgRobot
from logger import Logger

__author__ = "chengxiao.lcc"
__date__ = "2018-11-20 21:06:55"


class RequestTask(threading.Thread):
    """
    task config example:
    {
        "task_name": "android_installs_report",
        "start_date": "2018-11-11"
        "end_date": "2018-11-11"
        "request_url": "https://hq.appsflyer.com/xxx",
    }
    """
    def __init__(self, task_config, ding_talk_robot, lock):
        super(RequestTask, self).__init__()
        self.task_config = task_config
        self.logger = Logger(task_config["task_name"])
        self.ding_talk_robot = ding_talk_robot
        self.lock = lock

    def get_task_name(self):
        return self.task_config["task_name"]

    def data_request(self):
        request_url = self.task_config["request_url"]
        self.logger.logger.info("request data start with url: " + request_url)
        try:
            #Add by Daniel. Support download retry every xx minutes
            file_prefix = self.get_file_name()
            check_file_name = file_prefix + ".check_file"
            lock_file_name = file_prefix + ".lock_file"
            verfied = self.verify_check_file(check_file_name)
            if(verfied != 'downloaded'):
                locked = self.verify_put_lock_file(lock_file_name)
                if(locked != 'conflicted'):
                    data = requests.get(request_url)
                    data_frame = pandas.read_csv(io.StringIO(data.text), low_memory=False)
                    data_frame = self.data_process(data_frame)
                    self.write_to_csv(data_frame, file_prefix)
                else:
                    #process running
                    pass
            else:
                #already done
                pass
        except Exception as e:
            self.logger.logger.error("Data request failed: " + str(e))
            #Add by Daniel. Support download retry every xx minutes
            self.remove_lock_file(lock_file_name)
            #End add by Daniel. 
            task_name = self.task_config["task_name"]
            self.ding_talk_robot.send_msg(
                "Task name: %s, error type: %s, error msg: %s" % (task_name, type(e), str(e)), task_name)

    

    def data_process(self, data_frame):
        data_frame.rename(columns=lambda x: x.lower().replace(" ", "_"), inplace=True)
        data_frame.drop(data_frame.columns[0:1], axis=1)
        self.logger.logger.info("Data has been processed")
        return data_frame

##add by daniel
    def verify_check_file(self,check_file_name):
        if(os.path.isfile(check_file_name)):
            self.logger.logger.info("File has been downloaded: " + check_file_name)
            return 'downloaded'
        else:
            self.logger.logger.info("Not downloaded, move on: " + check_file_name)
            return 'no'

    def verify_put_lock_file(self,lock_file_name):
        if(os.path.isfile(lock_file_name)):
            self.logger.logger.info("Lock file exists, cancel download: " + lock_file_name)
            return 'conflicted'
        else:
            with open(lock_file_name, "w") as lock_file:
                lock_file.write("locked")
                lock_file.close()
            self.logger.logger.info("Create lock file and continue download: " + lock_file_name)
        return 'got_lock'

    def remove_lock_file(self,lock_file_name):
        os.remove(lock_file_name)
        self.logger.logger.info("Remove lock file before exit: " + lock_file_name)
        return 'done'

    def get_file_name(self):
        cur_dir = os.path.dirname(os.path.realpath(__file__))

        start_date_str = self.task_config["start_date"]
        end_date_str = self.task_config["end_date"]

        if start_date_str != end_date_str:
            raise Exception('Support only one day')
        target_date = datetime.datetime.strptime(start_date_str, '%Y-%m-%d')
        dir_month = target_date.strftime('%Y%m')
        dir_date = target_date.strftime('%Y%m%d')

        month_dir_path = os.path.join(cur_dir, dir_month)
        date_dir_path = os.path.join(month_dir_path, dir_date)
        try:
            self.lock.acquire()
            if not os.path.exists(month_dir_path):
                os.mkdir(month_dir_path)
                self.logger.logger.info("Month dir %s has been created" % month_dir_path)

            if not os.path.exists(date_dir_path):
                os.makedirs(date_dir_path)
                self.logger.logger.info("Date dir has been created: " + date_dir_path)
            file_name = self.task_config["task_name"] + "_" + dir_date
            full_file_name = os.path.join(date_dir_path, file_name)
            self.lock.release()
        except OSError as e:
            self.logger.logger.error("Date dir created fail: " + str(e))
            raise e
        return full_file_name
##end add by daniel

    def write_to_csv(self, data_frame, file_prefix):
        # self.prepare_dir(self)
        full_file_name = file_prefix + ".csv"
        check_file_name = file_prefix + ".check_file"
        lock_file_name = file_prefix + ".lock_file"

        data_frame.to_csv(full_file_name, index=False, sep="\t", encoding="utf-8")
        self.logger.logger.info("Data file has been created: " + full_file_name)
        self.logger.logger.info("Data rows: %d" % data_frame.shape[0])

        if data_frame.shape[0] <= 1:
            task_name = self.task_config["task_name"]
            self.ding_talk_robot.send_msg(
                "Task name: %s get 0 data rows" % (task_name), task_name)

        # full_check_file_name = os.path.join(date_dir_path, check_file_name)
        with open(check_file_name, "w") as check_file:
            check_file.write("done")
            check_file.close()
        self.remove_lock_file(lock_file_name)
        self.logger.logger.info("Check file has been created: " + check_file_name)
        #else:
            # self.divide_several_days_data(data_frame)
            # Daniel: currently, this branch won't reach
            #pass

    def divide_several_days_data(self, data_frame):
        self.logger.logger.info("generate multiple files.")
        print(data_frame)
        pass

    def run(self):
        self.data_request()


class TaskManager:
    """ 
    Manager to construct all the tasks or single task.
    """
    def __init__(self, task_configs):
        self.task_configs = task_configs
        self.logger = Logger("task_manager")

        alarm_white_list = task_configs["alarm_white_list"]
        self.ding_talk_robot = DingTalkMsgRobot(alarm_white_list)
        self.lock = threading.Lock()

    def run_batch_task(self, run_mode):
        task_list = []
        request_tasks = self.task_configs["request_tasks"]

        yesterday = datetime.datetime.now() - datetime.timedelta(days=1)
        default_date = yesterday.strftime('%Y-%m-%d')

        for task_name in request_tasks.keys():
            request_url = self.gen_request_url(task_name, default_date, default_date)
            task_config = {
                "task_name": task_name,
                "start_date": default_date,
                "end_date": default_date,
                "request_url": request_url
            }
            task_list.append(RequestTask(task_config, self.ding_talk_robot, self.lock))

        if run_mode == "concurrent":
            self.logger.logger.info("concurrent mode start...")
            start_time = datetime.datetime.now()

            for task in task_list:
                task.start()
                time.sleep(1)
            for task in task_list:
                task.join()

            end_time = datetime.datetime.now()
            time_diff = end_time - start_time
            seconds = time_diff.days * 24 * 3600 + time_diff.seconds
            self.logger.logger.info("concurrent mode end, time used: %d seconds" % seconds)

        if run_mode == "sequence":
            self.logger.logger.info("sequence mode start...")
            start_time = datetime.datetime.now()

            for task in task_list:
                task.data_request()

            end_time = datetime.datetime.now()
            self.logger.logger.info("sequence mode end, time used: %d seconds" % (end_time - start_time).seconds)

    def run_single_task(self, task_name, start_date, end_date):
        task_config = {"task_name": task_name,
                       "start_date": start_date,
                       "end_date": end_date,
                       "request_url": self.gen_request_url(task_name, start_date, end_date)}
        request_task = RequestTask(task_config, self.ding_talk_robot, self.lock)
        request_task.data_request()

    def gen_request_url(self, task_name, start_date, end_date):
        request_tasks = self.task_configs["request_tasks"]

        platform_id = request_tasks[task_name]["platform_id"]
        report_type = request_tasks[task_name]["report_type"]
        api_postfix = request_tasks[task_name]["api_url_postfix"]

        api_token = self.task_configs["api_token"]

        request_url_prefix = self.task_configs["api_url_prefix_template"] \
            .format(platform_id=platform_id, report_type=report_type, api_token=api_token,
                    start_date=start_date, end_date=end_date)

        return request_url_prefix + api_postfix

    def del_history_temp_dir(self):
        recent_date = datetime.datetime.now() - datetime.timedelta(days=7)
        dir_month = recent_date.strftime('%Y%m')
        dir_date = recent_date.strftime('%Y%m%d')

        cur_dir = os.path.dirname(os.path.realpath(__file__))
        month_dir = os.path.dirname(os.path.join(cur_dir, dir_month))
        date_dir = os.path.join(cur_dir, dir_month, dir_date)

        if os.path.exists(date_dir):
            for file_name in os.listdir(date_dir):
                os.remove(os.path.join(date_dir, file_name))
                self.logger.logger.info("Temp data file has been deleted: " + file_name)
            os.removedirs(date_dir)
            self.logger.logger.info("Temp date dir has been deleted: " + date_dir)
        if not os.listdir(month_dir):
            os.removedirs(month_dir)
            self.logger.logger.info("Temp month dir has been deleted: " + date_dir)

    def run_history_data(self, task_type, task_name, start_date, end_date):
        self.logger.logger.info("Start to request history data, date range: %s - %s" % (start_date, end_date))
        request_tasks = self.task_configs["request_tasks"]
        try:
            start_datetime = datetime.datetime.strptime(start_date, "%Y-%m-%d")
            end_datetime = datetime.datetime.strptime(end_date, "%Y-%m-%d")

            while (end_datetime - start_datetime).days >= 0:
                data_date = end_datetime.strftime('%Y-%m-%d')

                if task_type == "history_all":
                    for configured_task_name in request_tasks.keys():
                        self.run_single_task(configured_task_name, data_date, data_date)
                elif task_type == "history_single":
                    self.run_single_task(task_name, data_date, data_date)

                end_datetime = end_datetime - datetime.timedelta(days=1)
                self.logger.logger.info("date %s finished" % data_date)
        except Exception as e:
            self.logger.logger.error("Request history data error: " + str(e))

if __name__ == "__main__":
    pass
