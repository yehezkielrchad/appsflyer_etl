#!/usr/bin/python
# -*- coding: UTF-8 -*-
from logger import Logger

__author__ = "chengxiao.lcc"
__date__ = "2017-05-17 14:42:47"


class FileOperator:
    """
    File reader and writer
    """
    def __init__(self):
        self.logger = Logger("common")

    def read_file_to_str(self, file_path):
        try:
            with open(file_path, "r") as source_file:
                file_content = source_file.read()
            source_file.close()
            return file_content
        except Exception as e:
            self.logger.logger.error("read file to string error: " + str(e))
            return None

    def read_file_to_line_list(self, file_path):
        try:
            with open(file_path, "r") as source_file:
                file_lines = source_file.readlines()
            source_file.close()
            return file_lines
        except Exception as e:
            self.logger.logger.error("read file to line list error: " + str(e))
            return None

if __name__ == "__main__":
    pass
