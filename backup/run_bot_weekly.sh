#!/bin/bash

#get yesterday in CentOS
yesterday=$(date -d '-1 day' '+%Y-%m-%d')
7daysago =$(date -d '-7 day' '+%Y-%m-%d')
id_platform_android="id.dana"
id_platform_ios="id1437123008"
home="/home/habibi/appsflyer"
date
printf "\nWeekly $7daysago until $yesterday report will be retrieved..  bot started..."

#WEEKLY REPORTS WILL BE EXECUTED FOR BOTH ORGANIC & NONORGANIC IN-APP-EVENTS REPORT, FOR BOTH ANDROID & IOS

#ORGANIC REPORTS
printf "\nOrganic Installs Reports\n"
 python3.6 $home/appsflyer_bot.py $id_platform_android organic_in_app_events_report $yesterday $7daysago af_complete_registration
#python3.6 appsflyer_bot.py $id_platform_android organic_installs_report $yesterday $yesterday af_complete_registration
 python3.6 $home/appsflyer_bot.py $id_platform_ios organic_in_app_events_report $yesterday $7daysago af_complete_registration
#python3.6 appsflyer_bot.py $id_platform_ios organic_installs_report $yesterday $yesterday af_complete_registration


#NON ORGANIC REPORTS
printf "\n\nNon Organic Installs Reports\n"
 python3.6 $home/appsflyer_bot.py $id_platform_android in_app_events_report $yesterday $7daysago af_complete_registration
#python3.6 appsflyer_bot.py $id_platform_android installs_report $yesterday $yesterday af_complete_registration
 python3.6 $home/appsflyer_bot.py $id_platform_ios in_app_events_report $yesterday $7daysago af_complete_registration
#python3.6 appsflyer_bot.py $id_platform_ios installs_report $yesterday $yesterday af_complete_registrationø

printf "\n\nWeekly $7daysago until $yesterday report is done\n\n==========================================================================================="


