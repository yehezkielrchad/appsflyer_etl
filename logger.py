#!/usr/bin/python
# -*- coding: UTF-8 -*-
import logging
import os

from logging import handlers

from os.path import *

__author__ = "chengxiao.lcc"
__date__ = "2018-11-20 19:40:30"


class Logger:
    level_relations = {
        'debug': logging.DEBUG,
        'info': logging.INFO,
        'warning': logging.WARNING,
        'error': logging.ERROR,
        'critical': logging.CRITICAL
    }

    def __init__(self, file_name, level='info', when='MIDNIGHT', backup_count=3,
                 fmt='%(asctime)s - %(pathname)s[line:%(lineno)d] - %(levelname)s: %(message)s'):
        self.logger = logging.getLogger(file_name)
        self.logger.setLevel(self.level_relations.get(level))

        format_str = logging.Formatter(fmt)

        stream_handler = logging.StreamHandler()
        stream_handler.setFormatter(format_str)

        log_dir = join(dirname(abspath(__file__)), "logs")

        if not exists(log_dir):
            os.makedirs(log_dir)
        log_file_name = join(log_dir, file_name) + ".log"

        file_handler = handlers.TimedRotatingFileHandler(
            filename=log_file_name, when=when, backupCount=backup_count, encoding='utf-8')
        file_handler.setFormatter(format_str)

        self.logger.addHandler(stream_handler)
        self.logger.addHandler(file_handler)


if __name__ == "__main__":
    pass
