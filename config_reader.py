#!/usr/bin/python
# -*- coding: UTF-8 -*-
import json
from os.path import *

from file_operate import FileOperator
from logger import Logger

__author__ = "chengxiao.lcc"
__date__ = "2018-11-21 10:10:04"


class ConfigReader:
    """
    Read and load json config file
    """
    def __init__(self):
        current_dir = dirname(abspath(__file__))
        self.config_file_name = join(current_dir, "task_config.json")
        self.logger = Logger("common")
        self.config_content = None

    def parse_and_get_config(self):
        """
        Get config content
        :return: content of json
        """
        if exists(self.config_file_name):
            file_operator = FileOperator()
            file_content = file_operator.read_file_to_str(self.config_file_name)
            self.config_content = json.loads(file_content)
            return self.config_content
        else:
            self.logger.logger.error("config file does not exists: " + self.config_file_name)

    def print_config(self):
        if self.config_content is not None:
            print(self.config_content)
        else:
            print("config content need be init.")

if __name__ == "__main__":
    pass
