import requests
from datetime import date
import datetime
import io
import pandas as pd
import sys
import os,errno
from pathlib import Path

api_key = '2e83b563-12d0-4b99-921e-85ecea8c00ac'

id_platform = sys.argv[1]
report_type = sys.argv[2]
start_date = sys.argv[3]
end_date = sys.argv[4]
event_name = sys.argv[5]

if id_platform == 'id1437123008':
	platform = 'ios'
elif id_platform == 'id.dana':
	platform = 'adr'

dirmonth = datetime.datetime.strptime(start_date, '%Y-%m-%d').strftime('%Y%m')
dirdate = datetime.datetime.strptime(start_date, '%Y-%m-%d').strftime('%Y%m%d')

def get_data(id_platform,report_type,start_date,end_date,event_name):
	api_link = 'https://hq.appsflyer.com/export/'+id_platform+'/'+report_type+'/v5?api_token=' + \
				api_key +'&from=' + start_date + '&to=' + end_date+'&event_name=' + event_name 
	data = requests.get(api_link)
	return data

# def get_ios_data():
#     api_link = 'https://hq.appsflyer.com/export/id1437123008/organic_in_app_events_report/v5?api_token=' + \
#                 api_key +'&from=' + start_date + '&to=' + end_date+'&event_name=' + event_name 
#     ios_data = requests.get(api_link)
#     return ios_data

# def get_android_data():
#     api_link = 'https://hq.appsflyer.com/export/id.dana/organic_in_app_events_report/v5?api_token=' + \
#                 api_key +'&from=' + start_date + '&to=' + end_date+'&event_name=' + event_name 
#     android_data = requests.get(api_link)
#     return android_data

def write_to_files():
	data = get_data(id_platform,report_type,start_date,end_date,event_name)	    
	df = pd.read_csv(io.StringIO(data.text))

	if not os.path.exists(dirmonth) and not  os.path.exists(dirmonth + '/'+ dirdate):
		try:
			os.mkdir(dirmonth)
			print('Month folder '+dirmonth+ ' has been created.')
			os.makedirs(dirmonth+'/'+dirdate)
			print('Date folder '+dirdate+' has been created.')
			
			df.to_csv(dirmonth+'/'+dirdate+'/'+platform + '_' + report_type + '_' + dirdate+'.csv')
		    # print(platform + '_' + report_type + '.csv')
			print(dirmonth+'/'+dirdate+'/'+platform + '_' + report_type + '_' + dirdate+'.csv has been created.' )
		except OSError as e:
			if e.errno != errno.EEXIST:
				raise 
	else: 
		os.path.exists(dirmonth) and os.path.exists(dirmonth + '/'+ dirdate)
		df.to_csv(dirmonth+'/'+dirdate+'/'+platform + '_' + report_type + '_' + dirdate+'.csv')
		print(dirmonth+'/'+dirdate+'/'+platform + '_' + report_type + '_' + dirdate+'.csv has been created.' )



def update_header():
	df = pd.read_csv(dirmonth+'/'+dirdate+'/'+platform + '_' + report_type + '_' + dirdate+'.csv')
# lowercase column name	
	df.columns = map(str.lower, df.columns)
# remove white chars in column name
	df.columns = df.columns.str.replace(' ','_')
	# print(ios_df.iloc[0])
	df.to_csv(dirmonth+'/'+dirdate+'/'+platform + '_' + report_type + '_' + dirdate+'.csv')
	print(dirmonth+'/'+dirdate+'/'+platform + '_' + report_type + '_' + dirdate+' column name has been updated.')


#     android_df = pd.read_csv('android.csv')
#     android_df['id'] = [random.randint(0,10000) for x in range(android_df.shape[0])]
#     print(android_df.iloc[0])


#     android_df = pd.read_csv('/Users/muhammadhabibi/android.csv')
#     print(android_df.iloc[0])

    
def main():
	write_to_files()	
	print('1')
	update_header()
	print('2')

	#read_csv

	# print(platform + '_' + report_type + '_' + str(today) + '.csv')
# get_data(id_platform,report_type,start_date,end_date,event_name)
#    print("3")
#    update_header()
    
if __name__ == "__main__":
	main()

