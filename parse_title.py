#!/usr/bin/python
import sys

def parse_to_delimiter(cols):
	print ','.join(cols)

def parse_to_ddl(cols):
	print "%s\tSTRING\tCOMMENT\t\'%s\'" % (cols[0],cols[0])
	for each_col in cols[1:]:
		print ",%s\tSTRING\tCOMMENT\t\'%s\'" % (each_col,each_col)

if __name__=='__main__':
	mode = sys.argv[1]
	for each in sys.stdin:
		columns = each.strip('\n').split('\t')
	if mode == 'delimiter':
		parse_to_delimiter(columns)
	elif mode == 'ddl':
		parse_to_ddl(columns)

		
