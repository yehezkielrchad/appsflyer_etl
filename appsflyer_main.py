#!/usr/bin/python
# -*- coding: UTF-8 -*-

import sys

from config_reader import ConfigReader
from request_task import TaskManager

__author__ = "chengxiao.lcc"
__date__ = "2018-11-21 10:12:48"

if __name__ == "__main__":
    config_reader = ConfigReader()
    task_configs = config_reader.parse_and_get_config()

    task_manager = TaskManager(task_configs)

    task_type = sys.argv[1]
    if task_type == "batch":
        mode = sys.argv[2]
        task_manager.run_batch_task(mode)
        task_manager.del_history_temp_dir()
    elif task_type == "single":
        task_name = sys.argv[2]
        start_date = sys.argv[3]
        end_date = sys.argv[4]
        task_manager.run_single_task(task_name, start_date, end_date)
    elif task_type == "history_all":
        start_date = sys.argv[2]
        end_date = sys.argv[3]
        task_manager.run_history_data(task_type, None, start_date, end_date)
    elif task_type == "history_single":
        task_name = sys.argv[2]
        start_date = sys.argv[3]
        end_date = sys.argv[4]
        task_manager.run_history_data(task_type, task_name, start_date, end_date)
