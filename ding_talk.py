#!/usr/bin/python
# -*- coding: UTF-8 -*-
import requests
import json

from logger import Logger

__author__ = "chengxiao.lcc"
__date__ = "2018-11-27 11:08:48"


class DingTalkMsgRobot:
    def __init__(self, alarm_white_list):
        self.log = Logger("ding_talk_robot")
        self.web_hook_url = "https://oapi.dingtalk.com/robot/send?access_token={token}"
        self.robot_token = "f3e8195554b4e6483517cf6101cb8628f770075b2dcced00e195031dbe64ebcb"
        self.alarm_white_list = alarm_white_list

    def send_msg(self, msg, task_name):
        if task_name in self.alarm_white_list:
            return

        msg = "Attention! Appsflyer data request fail! It will retry later, but in case failed many times, you'd better login server to have a check. " + msg
        msg_body = {
            "msgtype": "text",
            "text": {
                "content": msg
            },
            "at": {
                "atMobiles": [
                    "17717010630"
                ],
                "isAtAll": False
            }
        }

        headers = {'content-type': 'application/json'}
        target_url = self.web_hook_url.format(token=self.robot_token)

        try:
            response = requests.post(target_url, data=json.dumps(msg_body), headers=headers)
            self.log.logger.info("DingTalk message sent, response: " + response.text)
        except Exception as e:
            self.log.logger.error("Send DingTalk message error: " + str(e))

if __name__ == "__main__":
    pass
