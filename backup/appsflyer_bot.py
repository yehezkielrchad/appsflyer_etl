from logging import handlers

import requests
from datetime import date, datetime, timedelta
import datetime
import io
import pandas as pd
import sys
import os, errno
import logging
from pathlib import Path

# ====================================================================================================================
# API KEY
# MAX LIMITATION COUNT      : 200.000 ROWS                              <-- NOT SOLVED TO ITERATE IF DATA >200K
# OLDEST AVAILABLE DATA         : 90 DAYS AGO                               <-- ONLY GET DATA FROM NOVEMBER 1ST
# MAX REQUESTS              : 24 / DAY / APP, 120 PER ACCOUNT IN TOTAL
api_key = '2e83b563-12d0-4b99-921e-85ecea8c00ac'
# ====================================================================================================================


class Logger:
    level_relations = {
        'debug': logging.DEBUG,
        'info': logging.INFO,
        'warning': logging.WARNING,
        'error': logging.ERROR,
        'critical': logging.CRITICAL
    }

    def __init__(self, file_name, level='info', when='MIDNIGHT', backup_count=3,
                 fmt='%(asctime)s - %(pathname)s[line:%(lineno)d] - %(levelname)s: %(message)s'):
        self.logger = logging.getLogger(file_name)
        self.logger.setLevel(self.level_relations.get(level))

        format_str = logging.Formatter(fmt)

        stream_handler = logging.StreamHandler()
        stream_handler.setFormatter(format_str)

        log_dir = os.path.split(os.path.realpath(__file__))[0] + os.path.sep + "logs"
        if not os.path.exists(log_dir):
            os.makedirs(log_dir)
        log_file_name = log_dir + os.path.sep + file_name + ".log"

        file_handler = handlers.TimedRotatingFileHandler(
            filename=log_file_name, when=when, backupCount=backup_count, encoding='utf-8')
        file_handler.setFormatter(format_str)

        self.logger.addHandler(stream_handler)
        self.logger.addHandler(file_handler)


# change current directory
os.chdir("/home/habibi/appsflyer")
# read user input from parameter
id_platform = sys.argv[1]
report_type = sys.argv[2]
start_date = sys.argv[3]
end_date = sys.argv[4]
event_name = sys.argv[5]

log = Logger("appsflyer_data_request")

# change variable
if id_platform == 'id1437123008':
    platform = 'ios'
elif id_platform == 'id.dana':
    platform = 'adr'

# define variable for folder month & subfolder date
dirmonth = datetime.datetime.strptime(start_date, '%Y-%m-%d').strftime('%Y%m')
dirdate = datetime.datetime.strptime(start_date, '%Y-%m-%d').strftime('%Y%m%d')


# dirmonth = start_date[:2]
# dirdate = start_date

def get_data(id_platform, report_type, start_date, end_date, event_name):
    # different report has different api link, code below should be refactored in future
    api_link = None
    if report_type == 'organic_in_app_events_report':
        api_link = 'https://hq.appsflyer.com/export/' + id_platform + '/' + report_type + '/v5?api_token=' + \
                   api_key + '&from=' + start_date + '&to=' + end_date + '&event_name=' + event_name
    elif report_type == 'organic_installs_report':
        api_link = 'https://hq.appsflyer.com/export/' + id_platform + '/' + report_type + '/v5?api_token=' + \
                   api_key + '&from=' + start_date + '&to=' + end_date + '&additional_fields=install_app_store,gp_referrer,gp_click_time,gp_install_begin'
    elif report_type == 'installs_report':
        api_link = 'https://hq.appsflyer.com/export/' + id_platform + '/' + report_type + '/v5?api_token=' + \
                   api_key + '&from=' + start_date + '&to=' + end_date + \
                   '&additional_fields=install_app_store,match_type,contributor1_match_type,contributor2_match_type,contributor3_match_type,device_category,gp_referrer,gp_click_time,gp_install_begin'
    elif report_type == 'in_app_events_report':
        api_link = 'https://hq.appsflyer.com/export/' + id_platform + '/' + report_type + '/v5?api_token=' + \
                   api_key + '&from=' + start_date + '&to=' + end_date + \
                   '&additional_fields=install_app_store,match_type,contributor1_match_type,contributor2_match_type,contributor3_match_type,device_category,gp_referrer,gp_click_time,gp_install_begin'

    log.logger.info("Start to request data, api_link: " + api_link)
    # enable logging while getting data from Appsflyer API
    # logging.basicConfig(level=logging.DEBUG)
    # get data from API
    data = requests.get(api_link)
    return data


def write_to_files():
    # get data from API
    data = get_data(id_platform, report_type, start_date, end_date, event_name)
    df = pd.read_csv(io.StringIO(data.text))

    # check if directory if not exists
    if not os.path.exists(dirmonth):
        os.mkdir(dirmonth)
        log.logger.info('Month folder ' + dirmonth + ' has been created.')

    # create subfolder hierarchy using month & date format
    if not os.path.exists(dirmonth + '/' + dirdate):
        try:
            os.makedirs(dirmonth + '/' + dirdate)
            log.logger.info('Date folder ' + dirdate + ' has been created.')

            df.to_csv(dirmonth + '/' + dirdate + '/' + platform + '_' + report_type + '_' + dirdate + '.csv',
                      index=False)
            # print(platform + '_' + report_type + '.csv')
            log.logger.info(
                dirmonth + '/' + dirdate + '/' + platform + '_' + report_type + '_' + dirdate + '.csv has been created.')
        except OSError as e:
            log.logger.error("Error when create sub folder", e)
            if e.errno != errno.EEXIST:
                raise
    elif os.path.exists(dirmonth + '/' + dirdate):
        df.to_csv(dirmonth + '/' + dirdate + '/' + platform + '_' + report_type + '_' + dirdate + '.csv', index=False)
        log.logger.info(
            dirmonth + '/' + dirdate + '/' + platform + '_' + report_type + '_' + dirdate + '.csv has been created.')


def update_header():
    # data = get_data(id_platform,report_type,start_date,end_date,event_name)
    df = pd.read_csv(dirmonth + '/' + dirdate + '/' + platform + '_' + report_type + '_' + dirdate + '.csv', \
                     delimiter=',', \
                     encoding="utf-8-sig", \
                     low_memory=False, \
                     dtype='unicode')

    # for testing offline
    # read file
    # df = pd.read_csv('201811/20181114/backup adr_installs_report_20181114.csv', \
    #   delimiter = '\t', \
    #   encoding="utf-8-sig",\
    #   low_memory=False, \
    #   dtype ='unicode')

    # lowercase column name
    df.columns = map(str.lower, df.columns)
    # remove white chars in column name
    df.columns = df.columns.str.replace(' ', '_')
    # remove space char in event value
    df['event_value'] = df['event_value'].str.replace(' ', '')
    #   df['Event Value'] = df['Event Value'].str.replace(' ','')
    # change datatype in string
    # df["campaign_id"] = df["campaign_id"].astype('int64')
    # df["adset_id"] = df["adset_id"].astype('int64')
    # df["ad_id"] = df["ad_id"].astype('int64')
    # skip first unused column
    df.drop(df.columns[0:1], axis=1)
    # save to file.csv
    df.to_csv(dirmonth + '/' + dirdate + '/' + platform + '_' + report_type + '_' + dirdate + '.csv', index=False,
              sep='\t', encoding='utf-8')
    # for testing offline data
    # df.to_csv('201811/20181114/outputnya.csv', index=False,sep='\t',encoding='utf-8')
    # df.write.option("sep","\t").option("header","true").csv('201811/20181114/test.txt')
    # notification
    file_path = dirmonth + '/' + dirdate + '/' + platform + '_' + report_type + '_' + dirdate
    log.logger.info(file_path + ' has been modified.')

    with open(file_path + '.check_file', 'w') as check_file:
        check_file.write("done")
    log.logger.info(file_path + ' check file has been created.')


def main():
    write_to_files()
    print('1\n')
    update_header()
    print('2\n\n')


# pd.show_versions(as_json=False)

if __name__ == "__main__":
    main()
